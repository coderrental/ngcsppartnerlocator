import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { AgmCoreModule, MarkerManager, GoogleMapsAPIWrapper } from '@agm/core';
import { HttpClientModule } from '@angular/common/http';
import { LoadingModule } from 'ngx-loading';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const appRoutes: Routes = [
  { path: '',            component: AppComponent },
  { path: 'project/:prj',  component: AppComponent },
  { path: 'project/:prj/partner/:pid',  component: AppComponent },
  { path: ':pid',  component: AppComponent }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    LoadingModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    ),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCVD3l4BLfVN1e72RwtfcCQVRmWXTc8nkI',
      libraries: ['geometry']
    })
  ],
  providers: [GoogleMapsAPIWrapper, MarkerManager],
  bootstrap: [AppComponent]
})
export class AppModule { }
