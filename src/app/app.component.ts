import { Component, ViewChild, ElementRef, AfterViewInit, OnInit, NgZone } from '@angular/core';
import { MouseEvent, MarkerManager, AgmMarker, GoogleMapsAPIWrapper, AgmMap, MapsAPILoader } from '@agm/core';
import { DataServiceService, Partner, marker, PartnerConversion } from './data-service.service';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { environment } from '../environments/environment';
import { Router, ActivatedRoute, Params, RoutesRecognized, NavigationEnd } from '@angular/router';
import { } from 'googlemaps';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/index';

declare var google: any;
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [MarkerManager]
})



export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild("searchCountry") searchCountryRef: ElementRef;
  @ViewChild('cspPartnerMap') cspPartnerMap: ElementRef;
  title: string = 'CSP Partner Locator';
  lat: number;
  lng: number;
  nLat: number;
  nLng: number;
  markers: marker[] = [];
  partnerListLargData: Partner[];
  partnerChunkedData: [Partner[]];
  partnerList: Partner[] = [];
  trafficLargeData: marker[];
  trafficChunkedData: [marker[]];
  trafficList: marker[] = [];
  loadInterval: any;
  loadTrafficInterval: any;
  loadAllDataInterval: any;
  isCspAdmin: boolean = false;
  isPartnerLoaded: boolean = false;
  isTrafficLoaded: boolean = false;
  loading: boolean = true;
  iconAPI: string = environment.iconAPI;
  searchKey: string = '';
  searchResult: Partner[] = [];
  navigateMarkerPartner: Partner;
  navigateMarker: AgmMarker;
  showSearch: boolean = false;
  pId: number = 0;
  project: string = '';
  isFirstLoad: boolean = true;
  searchCountry: string[] = [];
  searchRadius = [50, 100, 200, 500, 1000];
  sRadius: number = 0;
  sCityZip: string = '';
  sCountry: string = 'United States';
  sState: string = '';
  sLenType: string = 'miles';
  geocoder: any;
  searchAddressResult: Subject<any> = new Subject<any>();
  listCountry: any[];
  listPartnerConversion: PartnerConversion[] = [];

  constructor(protected _markerMan: MarkerManager, 
              protected _dataService: DataServiceService, 
              protected _gmApiWrapper: GoogleMapsAPIWrapper,
              private activeRoute: ActivatedRoute,
              private router: Router,
              private mapsAPILoader: MapsAPILoader) {
    this.isCspAdmin = typeof(this._getUrlParameter('iam')) != 'undefined' && this._getUrlParameter('iam') == 'cspadmin';
    
    setTimeout(() => {
      this.loading = false;
    }, 3000);
    
    this.loadAllDataInterval = setInterval(() => {
      if (this.isPartnerLoaded && this.isTrafficLoaded) {
        clearInterval(this.loadAllDataInterval);
        if (this.pId > 0) {
          /* let defaultP = this.partnerListLargData.filter(x => x.id == this.pId);
          if (defaultP.length > 0) {
            this.goToLocation(defaultP[0]);
          } */
        }
        else {
          if (this.partnerList.length == 0) {
            this._checkCurrentLocation();
            this.lazyLoadPartnerToMap();
          }
        }
        setTimeout(() => {
          this.loading = false;
        }, 3000);
        return;
      }
    }, 1000);
  }

  ngOnInit(): void {
    this._getTraffic();
    this._getCountryList();
    
    this.router.events.subscribe(val => {
      let gotPId: boolean = false;
      if (val instanceof RoutesRecognized) {
        if (typeof(val.state.root.firstChild.params['pid']) != 'undefined' && !isNaN(val.state.root.firstChild.params['pid'])) {
          gotPId = true;
          this.pId = +val.state.root.firstChild.params['pid'];
        }
        if (typeof(val.state.root.firstChild.params['prj']) != 'undefined') {
          this.project = val.state.root.firstChild.params['prj'];
        }
      }
      else if (val instanceof NavigationEnd) {
        gotPId = this.pId > 0;
        this.project = this.project != '' ? this.project : 'digitalchannel';
        if (this.project.toLowerCase() == 'forrtech') this.project = 'salesdemo';
        this._getPartnerData();
        if (this.pId > 0) {
          this._dataService.getPartnerById(this.project, this.pId).subscribe(x => {
            if (x != null && x.length > 0) {
              let defaultPartner = x[0];
              defaultPartner.latitude = Number(defaultPartner.latitude);
              defaultPartner.longitude = Number(defaultPartner.longitude);
              this.goToLocation(defaultPartner);
            }
          });
          /* let defaultP = this.partnerListLargData.filter(x => x.id == this.pId);
          if (defaultP.length > 0) {
            this.goToLocation(defaultP[0]);
          } */
        }
        else {
          console.log('load default');
        }
        if (!gotPId && this.pId == 0) this._checkCurrentLocation();
        
        this.isFirstLoad = false;
      }
    });

    this.router.events.subscribe(events => {
      
    })

    this.mapsAPILoader.load().then(() => {
      this.geocoder = new google.maps.Geocoder();
      this.searchAddressResult.subscribe(x => {
        if (x.lat() == 0 && x.lng() == 0) {
          this.loading = false;
          return;
        }
        let nLat = x.lat();
        let nLng = x.lng();

        this.goToLatLng(nLat, nLng);
  
        let loadingEl: HTMLElement = document.getElementById('csp-loading') as HTMLElement;
        loadingEl.click();
        this.loading = false;
      });
    });
  }

  cspMapClick($event: MouseEvent): void {
    if (!this.isCspAdmin) return;
    var trafficP = Math.floor(Math.random() * (150 - 1) + 1);
    var trafficD = 'Low';
    if (trafficP >= 50)
      trafficD = 'Medium';
    if (trafficP >= 100)
      trafficD = 'High';
    let newTraffic = {
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: false,
      traffic: trafficP,
      trafficDesc: trafficD
    };
    this.trafficLargeData.push(newTraffic);
    this.trafficList.push(newTraffic);
  }

  cspMapDragEnd(m: marker, $event: MouseEvent) {
    //console.log('dragEnd', m, $event);
  }

  doExportTraffic(): void {
    //this.exportAsExcelFile(this.markers, 'cspPartnerLocatorTraffic.xlsx');
    new Angular5Csv(this.trafficLargeData, 'ConversionTraffic');
  }

  onSearch($event): void {
    if (!this.isPartnerLoaded) return;
    var x = document.getElementById("infobox");
    
    if (this.searchKey.trim() == '') {
      this.searchResult = [];
      this.showSearch = false;
      if (x.style.display === "none") {
        x.style.display = "flex";
      }
      return;
    }
    if (x.style.display !== "none") {
        x.style.display = "none";
    }
    var lowerSearchKey = this.searchKey.toLowerCase();
    this.searchResult = this.partnerListLargData.filter( x => x.companyName.toLowerCase().indexOf(lowerSearchKey) != -1 || 
                                                              (x.address1 != null && x.address1.toLowerCase().indexOf(lowerSearchKey) != -1) ||
                                                              (x.emailAddress != null && x.emailAddress.toLowerCase().indexOf(lowerSearchKey) != -1));
    
  }

  goToLocation(navigatePartner: Partner): void {
    navigatePartner.latitude = Number(navigatePartner.latitude);
    navigatePartner.longitude = Number(navigatePartner.longitude);
    this.lat = navigatePartner.latitude;
    this.lng = navigatePartner.longitude;
    this.navigateMarkerPartner = navigatePartner;
    this.showSearch = true;
    if ((this.partnerListLargData == undefined || this.partnerListLargData.length == 0)) {
      if (this.project == 'digitalchannel' && (this.listPartnerConversion == undefined || this.listPartnerConversion.length == 0)) {
        this._loadPartnerConversionThenPartner(navigatePartner.latitude, navigatePartner.longitude);
      }
      else
        this._loadPartnerByDist(navigatePartner.latitude, navigatePartner.longitude);
      //this._loadPartnerByDist(navigatePartner.latitude, navigatePartner.longitude);
      /*
      this._dataService.getPartnerByDistance(this.project, navigatePartner.latitude, navigatePartner.longitude, 500).subscribe(x => {
        if (x.length > 0) {
          let max = 1000;
          let min = 300;
          x.forEach(p => {
            p.reach = Math.floor(Math.random() * (+max - +min) + +min);
          });
          this.partnerList = x;
        }
      });
      */
    }
  }

  goToLatLng(nLat: number, nLng: number): void {
    this.lat = nLat;
    this.lng = nLng;
  }

  toogleInforBox(){
    var x = document.getElementById("infobox");
    if (x.style.display === "none") {
        x.style.display = "flex";
    } else {
        x.style.display = "none";
    }
  }

  centerChanged($event): void {
    this.nLat = $event.lat;
    this.nLng = $event.lng;
  }

  boundsChanged($event): void {
    //console.log($event);
  }

  lazyLoadPartnerToMap(): void {
    //if (this.partnerListLargData == undefined || this.partnerListLargData.length == 0) return;
    this.lat = this.nLat;
    this.lng = this.nLng;
    this.mapsAPILoader.load().then(() => {
      //console.log('maps api loaded', this.lat, this.lng);
      const center = new google.maps.LatLng(this.lat, this.lng);
      this.loading = false;
      if ((this.partnerListLargData == undefined || this.partnerListLargData.length == 0)) {
        if (this.lat > 0 && this.lng > 0) {
          if (this.project == 'digitalchannel' && (this.listPartnerConversion == undefined || this.listPartnerConversion.length == 0)) {
            this._loadPartnerConversionThenPartner(this.lat, this.lng);
          }
          else
            this._loadPartnerByDist(this.lat, this.lng);
          //this._loadPartnerByDist(this.lat, this.lng);
          /*
          this._dataService.getPartnerByDistance(this.project, this.lat, this.lng, 500).subscribe(x => {
            if (x.length > 0) {
              let max = 1000;
              let min = 300;
              x.forEach(p => {
                p.reach = Math.floor(Math.random() * (+max - +min) + +min);
              });
              this.partnerList = x;
            }
          });
          */
        }
      }
      else {
        let tmp = this.partnerListLargData.filter(m => {
          let inMiles = this.sLenType == 'miles' ? 0.621371 : 1;
          const markerLoc = new google.maps.LatLng(m.latitude, m.longitude);
          const distanceRaw = google.maps.geometry.spherical.computeDistanceBetween(markerLoc, center);
          const distanceInKm =  (distanceRaw / 1000) * inMiles;
          let distanceRadius = this.sRadius == 0 ? 1000 : this.sRadius;
          if (distanceInKm < distanceRadius){
            return m;
          }
        });

        let max = this.listPartnerConversion.length;
        let min = 1;
        
        tmp.forEach(p => {
          var pConversion = this.listPartnerConversion.filter(x => x.PartnerId == p.id);
          if (typeof(pConversion[0]) != 'undefined') {
            pConversion = [];
            pConversion[0] = this.listPartnerConversion[Math.floor(Math.random() * (+max - +min) + +min)];
          }
          if (typeof(pConversion[0]) != 'undefined') {
            p.reach = pConversion[0].Reach;
            p.clicks = pConversion[0].Clicks;
            p.leads = pConversion[0].Leads;
            p.opportunities = pConversion[0].Opportunities;
            p.deals = pConversion[0].Deals;
          }
          else {
            p.reach = Math.floor(Math.random() * (+max - +min) + +min);
            p.clicks = Math.floor(Math.random() * (+max - +min) + +min);
            p.leads = Math.floor(Math.random() * (+max - +min) + +min);
            p.opportunities = Math.floor(Math.random() * (+max - +min) + +min);
            p.deals = Math.floor(Math.random() * (+max - +min) + +min);
          }

        });

        this.partnerList = tmp;
      }
      if (this.trafficLargeData != undefined && this.trafficLargeData.length > 0) {
        this.trafficList = this.trafficLargeData.filter(m => {
          let inMiles = this.sLenType == 'miles' ? 0.621371 : 1;
          const markerLoc = new google.maps.LatLng(m.lat, m.lng);
          const distanceRaw = google.maps.geometry.spherical.computeDistanceBetween(markerLoc, center);
          const distanceInKm =  (distanceRaw / 1000) * inMiles;
          let distanceRadius = this.sRadius == 0 ? 1000 : this.sRadius;
          if (distanceInKm < distanceRadius){
            return m;
          }
        });
      }
    });
  }

  getValidUrl(theUrl: string): string {
    if (theUrl == undefined || theUrl == '') return theUrl;
    let validUrl = theUrl.indexOf('http') != -1 || theUrl.indexOf('//') != -1 ? theUrl : '//' + theUrl;
    return validUrl;//validUrl.replace('http://', 'https://');
  }

  CspGoSearch(newAdd: string): void {
    //console.log(this.sLenType, this.sRadius);
    setTimeout(() => {
      this.lat = this.lat + 0.0001;
      this.lng = this.lng + 0.0001;
    }, 500);
    setTimeout(() => {
      this.lat = this.lat + 0.0001;
      this.lng = this.lng + 0.0001;
    }, 1000);
    
    if (this.geocoder == undefined) this.geocoder = new google.maps.Geocoder();
    this.loading = true;
    
    let address = `${this.sCountry} ${this.sState} ${this.sCityZip}`;
    if (newAdd != undefined && newAdd != '')
      address = newAdd;
    
    this._getGeoCoderSearch(address);
  }

  LoadRandom(type: string) {
    let max = 300;
    let min = 30;
    if (type == 'reach')
      return Math.random() * (+max - +min) + +min;
    return Math.random() * (+max - +min) + +min
  }

  _getGeoCoderSearch(address: string): void {
    this.geocoder.geocode( {'address': address}, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
          this.searchAddressResult.next(results[0].geometry.location);
      } else {
          console.log('Error - ', results, ' & Status - ', status);
      }
    });
  }

  _checkCurrentLocation(): void {
    if (this.project != undefined && this.project != null && this.project.toLowerCase() == 'salesdemo') {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
        });
      } else {
        alert("Geolocation is not supported by this browser.");
      }
      return;
    }
    this.lat = 40.267193;
    this.lng = -86.134903;
    if (this.partnerListLargData == undefined || this.partnerListLargData.length == 0) {
      if (this.project == 'digitalchannel' && (this.listPartnerConversion == undefined || this.listPartnerConversion.length == 0)) {
        this._loadPartnerConversionThenPartner(this.lat, this.lng);
      }
      else
        this._loadPartnerByDist(this.lat, this.lng);
      //this._loadPartnerByDist(this.lat, this.lng);
      /*
      this._dataService.getPartnerByDistance(this.project, this.lat, this.lng, 500).subscribe(x => {
        if (x.length > 0) {
          let max = 1000;
          let min = 300;
          x.forEach(p => {
            p.reach = Math.floor(Math.random() * (+max - +min) + +min);
          });
          this.partnerList = x;
        }
      });
      */
    }
    /*
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
    */
  }

  _getPartnerData(): void {
    this._dataService.loadPartnerConversion(this.project);
    this._dataService.getPartnerConversionObservable().subscribe(pcData => {
      if (this.project == 'digitalchannel' && (pcData == null || pcData.length == 0)) return;

      this.listPartnerConversion = pcData != null ? pcData : [];
      this._dataService.getPartnerByProject(this.project);
      this._dataService.getPartnersObservable().subscribe(dataPartner => {
        
        if (dataPartner.length == 0) return;
        dataPartner = dataPartner.filter(x => x.longitude != undefined && x.longitude != null && x.latitude != undefined && x.latitude != null);
        for (var i = 0; i < dataPartner.length; i++) {
          dataPartner[i].longitude = Number(dataPartner[i].longitude);
          dataPartner[i].latitude = Number(dataPartner[i].latitude);
        }
        if (this.navigateMarkerPartner == undefined)
          this.navigateMarkerPartner = dataPartner[0];

        this.partnerListLargData = dataPartner;
        //console.log('Valid partner (has lat/lng) count:', this.partnerListLargData.length);
        this.isPartnerLoaded = true;
        this._loadSearchData();
      });
    });
  }

  _getTraffic(): void {
    this._dataService.getTrafficObservable().subscribe(dataTraffic => {
      if (dataTraffic.length > 0) {
        this.trafficLargeData = dataTraffic;
        this.lazyLoadPartnerToMap();
      }
      this.isTrafficLoaded = true;
      //this._chunkLargeTrafficData();
      //this._loadTrafficToMap(0);
    });
  }

  _getCountryList(): void {
    this._dataService.getCountriesObservable().subscribe(cList => {
      if (cList.length == 0) return;
      this.listCountry = cList;
    });
  }

  _loadSearchData(): void {
    var rawCountryList = this.partnerListLargData.map(x => x.country);
    this.searchCountry = rawCountryList.filter(function(value, index, self) {
      return self.indexOf(value) === index;
    });
    //console.log(this.searchCountry);
  }

  _chunkLargeData(): void {
    if (typeof(this.partnerListLargData) == 'undefined' || this.partnerListLargData.length == 0) return;
    var pageSize = 100;
    this.partnerChunkedData = this._dataService.chunk(this.partnerListLargData, pageSize);
  }

  _chunkLargeTrafficData(): void {
    if (typeof(this.trafficLargeData) == 'undefined' || this.trafficLargeData.length == 0) return;
    var pageSize = 100;
    this.trafficChunkedData = this._dataService.chunk(this.trafficLargeData, pageSize);
  }

  _loadDataToMap(crrPage: number): void {
    if (typeof(this.partnerListLargData) == 'undefined' || this.partnerListLargData.length == 0) {
      this.isPartnerLoaded = true;
      return;
    }
    if (crrPage >= this.partnerChunkedData.length) return;

    this.isPartnerLoaded = true;
    this._loadPartnerLocation();
    /*
    this.loadInterval = setInterval(() => {
      if (crrPage >= this.partnerChunkedData.length) {
        clearInterval(this.loadInterval);
        this.isPartnerLoaded = true;
        this._loadPartnerLocation();
        console.log('All partners loaded');
        return;
      }
      this.partnerList = this.partnerList.concat(this.partnerChunkedData[crrPage]);
      crrPage = crrPage + 1;
    }, 500);
    */
  }

  _loadPartnerLocation(): void {
    if (this.pId <= 0) return;
    //var pObj = this.partnerList.filter(x => x.)
  }

  _loadTrafficToMap(crrPage: number): void {
    if (typeof(this.trafficLargeData) == 'undefined' || this.trafficLargeData.length == 0) {
      this.isTrafficLoaded = true;
      return;
    }
    if (crrPage >= this.trafficChunkedData.length) return;
    
    this.loadTrafficInterval = setInterval(() => {
      if (crrPage >= this.trafficChunkedData.length) {
        clearInterval(this.loadTrafficInterval);
        this.isTrafficLoaded = true;
        //console.log('All traffic loaded');
        return;
      }
      this.trafficList = this.trafficList.concat(this.trafficChunkedData[crrPage]);
      crrPage = crrPage + 1;
    }, 350);
  }

  _getLatLng(address: string): Observable<any> {
    console.log('Getting Address - ', address);
    let geocoder = new google.maps.Geocoder();
    return Observable.create(observer => {
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                observer.next(results[0].geometry.location);
                observer.complete();
            } else {
                console.log('Error - ', results, ' & Status - ', status);
                observer.next({});
                observer.complete();
            }
        });
    })
  }

  _getUrlParameter(sParam) {
    return decodeURIComponent(window.location.search.substring(1)).split('&')
     .map((v) => { return v.split("=") })
     .filter((v) => { return (v[0] === sParam) ? true : false })
     .reduce((acc:any,curr:any) => { return curr[1]; },undefined); 
  }

  _loadPartnerConversionThenPartner(lat: any, lng: any) {
    this._dataService.getPartnerConversionObservable().subscribe(pcData => {
      if (this.project == 'digitalchannel' && (pcData == null || pcData.length == 0)) {
        return;
      }
      this.listPartnerConversion = pcData;
      
      this._loadPartnerByDist(lat, lng);
    })
  }

  _loadPartnerByDist(lat: any, lng: any) {
    this._dataService.getPartnerByDistance(this.project, lat, lng, 500).subscribe(x => {
      if (x.length > 0) {
        let max = this.listPartnerConversion.length;
        let min = 1;
        
        x.forEach(p => {
          var pConversion = this.listPartnerConversion.filter(x => x.PartnerId == p.id);
          if (typeof(pConversion[0]) != 'undefined') {
            pConversion = [];
            pConversion[0] = this.listPartnerConversion[Math.floor(Math.random() * (+max - +min) + +min)];
          }
          if (typeof(pConversion[0]) != 'undefined') {
            p.reach = pConversion[0].Reach;
            p.clicks = pConversion[0].Clicks;
            p.leads = pConversion[0].Leads;
            p.opportunities = pConversion[0].Opportunities;
            p.deals = pConversion[0].Deals;
          }
          else {
            p.reach = Math.floor(Math.random() * (+max - +min) + +min);
            p.clicks = Math.floor(Math.random() * (+max - +min) + +min);
            p.leads = Math.floor(Math.random() * (+max - +min) + +min);
            p.opportunities = Math.floor(Math.random() * (+max - +min) + +min);
            p.deals = Math.floor(Math.random() * (+max - +min) + +min);
          }
        });

        this.partnerList = x;
        console.log(this.partnerList);
      }
    });
  }

  ngAfterViewInit(): void {
    
  }
}


