import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})

export class DataServiceService {
  private partnerList$: BehaviorSubject<Partner[]> = <BehaviorSubject<Partner[]>> new BehaviorSubject(new Array<Partner>());
  private trafficList$: BehaviorSubject<marker[]> = <BehaviorSubject<marker[]>> new BehaviorSubject(new Array<marker>());
  private countryList$: BehaviorSubject<CspCountry[]> = <BehaviorSubject<CspCountry[]>> new BehaviorSubject(new Array<CspCountry>());
  private partnerConversionList$: BehaviorSubject<PartnerConversion[]> = <BehaviorSubject<PartnerConversion[]>> new BehaviorSubject(new Array<PartnerConversion>());


  constructor(protected _http: HttpClient) { 
    
    /* this._http.get<Partner[]>(environment.baseFolderRoute + 'assets/data/partnerlocation.json')
              .subscribe(resData => {this.partnerList$.next(resData)}); */
    
    this._http.get<marker[]>(environment.baseFolderRoute + 'assets/data/cspTraffic.json')
              .subscribe(resData => {this.trafficList$.next(resData)});

    this._http.get<any[]>(environment.baseFolderRoute + 'assets/data/countries.json')
              .subscribe(resData => {
                let tmpCountryList: CspCountry[] = [];
                for (var key in resData) {
                  var tmpC = new CspCountry();
                  tmpC.code = key;
                  tmpC.name = resData[key];
                  tmpCountryList.push(tmpC);
                }
                tmpCountryList.sort(function(a,b) {return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0);} ); 
                this.countryList$.next(tmpCountryList);
              });
  }

  getPartnerByProject(prj: string) {
    this._http.get<Partner[]>(`${environment.iconAPI}api/${prj}/prod/partnerlocator/partnerlist`)
			.subscribe(x => {
        let max = 1000;
        let min = 300;
        x.forEach(p => {
          p.reach = Math.floor(Math.random() * (+max - +min) + +min);
        });
        this.partnerList$.next(x);
      })
			;
  }

  getPartnerById(prj: string, cspId: number): Observable<Partner[]> {
    return this._http.get<Partner[]>(`${environment.iconAPI}api/${prj}/prod/partnerlocator/getbyparameter?param=cspid&value=${cspId}`)
    .pipe(
      
    );
  }

  getPartnerByDistance(prj: string, lat: number, lng: number, dist: number): Observable<Partner[]> {
    let options = {
      "latitude": lat,
      "longitude": lng,
      "distance": dist
    };
    return this._http.post<Partner[]>(`${environment.iconAPI}api/${prj}/prod/partnerlocator/getbydistance`, options)
    .pipe(
      //catchError(this.handleError('addHero', hero))
    );
  }

  getPartnerConversion(prj: string): Observable<PartnerConversion[]> {
    let tmp: PartnerConversion[] = [];
    if (prj != 'digitalchannel') return null;
    return this._http.get<PartnerConversion[]>(`${environment.iconAPI}exportdata/${prj}/prod/partner_conversion.json`);
  }

  loadPartnerConversion(prj: string) {
    this.getPartnerConversion(prj).subscribe(x => {
      this.partnerConversionList$.next(x);
    })
  }

  getPartnerConversionObservable(): Observable<PartnerConversion[]> {
		return this.partnerConversionList$.asObservable();
  }

  getPartnersObservable(): Observable<Partner[]> {
		return this.partnerList$.asObservable();
  }

  getTrafficObservable(): Observable<marker[]> {
		return this.trafficList$.asObservable();
  }

  getCountriesObservable(): Observable<any[]> {
		return this.countryList$.asObservable();
  }
  
  private handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
  }

  chunk(pData: any[], chunkSize: number): [any[]] {
    var R: any = [];
    for (var i=0; i<pData.length; i+=chunkSize)
        R.push(pData.slice(i,i+chunkSize));
    return R;
  }
}

export class Partner {
  id: number;
  companyName: string;
  address1: string;
  address2: string;
  city: string;
  zipCode?: number;
  state: string;
  country: string;
  website_Url: string;
  latitude?: number;
  longitude?: number;
  emailAddress: string;
  telephone: string;
  deeplink_Landingpage_Url: string;
  unify_Certifications: string;
  csP_Flow_Coins: string;
  reach?: number;
  clicks?: number;
  leads?: number;
  opportunities?: number;
  deals?: number;

  constructor() {
    this.companyName = ''; this.address1 = ''; this.emailAddress = ''; this.website_Url = '';
  }
}

// just an interface for type safety.
export class marker {
	lat: number;
	lng: number;
	label?: string;
  draggable: boolean;
  traffic: number;
  trafficDesc: string;
}

export class CspCountry {
  code: string;
  name: string;
}

export class PartnerConversion {
  PartnerId: number;
  Reach: number;
  Clicks: number;
  Leads: number;
  Opportunities: number;
  Deals: number;
}